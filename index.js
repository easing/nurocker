var NuRocker  = require('./lib/NuRocker'),
    extractor = require('./lib/extractor'),
    Saver     = require('./lib/saver');


var config = {
    source:     'data/sample.zip',                   // Название архива с данными
    extract_to: 'extracted',                         // Директория, в которую будем распаковывать файлы
    mongo:      'mongodb://localhost:27017/nurocker' // Connection string для подключения монги
};

// Запускаем счетчики для замеров времени
console.time('total');
console.time('unzip');

var onParse = function(lots){
    console.timeEnd('parse');
    console.log('Найдено лотов: ' + lots.length);

    new Saver( config.mongo, +new Date() )
        .insert(lots, function(result){
            console.log(result);
            console.timeEnd('total');
        });

};

// Распаковываем архив и обрабатываем данные
extractor.extract(config.source, config.extract_to, function(out){

    console.log('Архив распакован');
    console.timeEnd('unzip');
    console.log('Ждем 5 секунд и запускаем обработку');


    // Добавляем задержку, чтобы архиватор успел закончить свою работу с большим архивом
    // TODO: придумать как обойтись без костыля
    setTimeout(function(){

        console.time('parse');

        new NuRocker(out, onParse).process();

    }, 5000);

});


