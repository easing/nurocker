var striptags = require('sanitize-html');

/**
 * Очищачет html, оставляя только пару разрешенных тэгов
 *
 * @returns {Sanitizer}
 * @constructor
 */
var Sanitizer = function(allowed){

    allowed = allowed || {
            allowedTags: [ 'b', 'strong', 'i', 'em', 'u', 'a', 'img' ],
            allowedAttributes: {
                'a': [ 'href' ],
                'img': [ 'src', 'alt' ]
            },
            allowedSchemesByTag: {
                img: [ 'http' ],
                a: [ 'http', 'https' ]
            },
            transformTags: {
                'strong': 'b',
                'em': 'i'
            },
            exclusiveFilter: function(frame) {
                return frame.tag === 'p' && !frame.text.trim();
            }
        };

    /**
     * Оставляет только разрешенные на 24au.ru тэги
     * @param html
     * @returns {*}
     */
    this.clean = function(html){
        return striptags(this.addNewLines(html), allowed);
    };

    /**
     * Добавляет переводы строк после нужных тэгов
     *
     * @param html
     * @returns {void|string|XML|*}
     */
    this.addNewLines = function(html){

        var blockTags = new RegExp('<br(\s*)?/?>|</(ol|ul)(\s*)>', 'gi');
        var pTag = new RegExp('</(p|li)(\s*)>', 'gi');

        html = html.replace(blockTags, '$&\r\n');
        html = html.replace(pTag, '$&\r\n\r\n');

        return html;
    };

    /**
     * Конвертирует разрешенные тэги в bb-кода
     */
    this.convertToBBcodes = function(html){

        var html = html.replace( new RegExp('<(/?[b|i|img](\s*))>', 'gi') , '[$1]');

        return html;

    };

    return this;
};


module.exports = new Sanitizer();
