var unzip = require('unzip'),
    fs    = require('fs'),
    path  = require('path');

module.exports = {

    /**
     * Распаковывает архив в нужную директорию и возвращает путь до неё
     *
     * @param  {string} _path   - относительный путь к архиву с данными
     * @param  {string} _output - относительный путь к папке, в которую распаковываем файлы
     *
     * @return {string} путь до папки с распакованными файлами
     */
    extract: function(_path, _output, callback) {

        // Полный путь до архива
        _path = path.resolve(_path);

        // Название архива
        var basename = path.basename(_path, '.zip');

        var extract_to = path.resolve(path.join(_output, basename))

        // Распаковываем архив
        var stream = fs.createReadStream(_path).pipe(unzip.Extract({path: extract_to}));

        stream.on('finish', function(err){
            console.log('Архив ' + basename + ' распакован');
            callback(extract_to);
        });
    }
};
