var path = require('path'),
    fs   = require('fs');

var Iconv = require('iconv').Iconv;
var translator = new Iconv('CP1251', 'UTF-8');

var parse = require('csv-parse');


/**
 * TODO: узнать бывает ли в архиве несколько файлов
 * и если да, реализовать корректную обработку такой ситуации
 */

/**
 * Читает папку c архивом лотов с Молотка
 * и возвращает коллекцию лотов с картинками и параметрами
 *
 * @param path - путь к папке с архивом лотов
 * @param callback - что делать с обработанными данными
 * @constructor
 */
function NuRocker(_path, callback) {

    this.folder = _path;
    this.data_folder = path.join(_path, 'item_info');

    // Свойства, которые не нужно обрабатывать
    this.excluded_columns = ['Id lota', 'Vkladka'];

    // Коллекция свойств
    this.columns = {};

    this.lots = [];

    this.stream = null;

    this.onFinish = callback;

    return this;
};

/**
 * Запускает обработку всех csv-файлов
 */
NuRocker.prototype.process = function(){
    this.getDataFiles().forEach(this.processFile.bind(this));
};

/**
 * Возвращает список csv-файлов в корне рабочей директории
 */
NuRocker.prototype.getDataFiles = function() {
    return fs.readdirSync(this.folder).filter(function(filename) {
        return path.extname(filename) === '.csv';
    });
};

/**
 * Читает содержимое файла, конвертирует его в utf-8 и запускает парсер.
 * По окончанию работы файла будет вызван стандартный callback
 *
 * @param file
 */
NuRocker.prototype.processFile = function(file) {

    var nr = this;
    var parser = parse({delimiter: ';', columns: true}, this.processLots.bind(this));

    this.stream = fs.createReadStream(path.join(this.folder, file)).pipe(translator).pipe(parser);

    this.stream.on('end', function(){
        nr.onFinish(nr.lots);
    });
};

/**
 * Запускает обработку всех лотов
 *
 * @param err
 * @param data
 */
NuRocker.prototype.processLots = function(err, data){
    var nr = this;

    data.forEach(function(lot){
        var processed_lot = nr.processLot(lot);

        if(processed_lot) {
            nr.lots.push(processed_lot);
        }
    });
};

/**
 * Обрабатывает данные из csv-строки и возвращает нормализованный лот
 *
 * @param data
 *
 * @return {}
 */
NuRocker.prototype.processLot = function(data) {
    var nr = this;

    // Распарсенный лот
    var processed = {
        'Id' : data['Id lota'],
        'Vkladka': data['Vkladka'],
        'properties' : {},
        'missed_properties': {},
        'images': [],
        'description': ''
    };

    Object.keys(data).forEach(function(key){
        var property = nr.getPropertyData(key);

        if(!property) {
            return;
        }

        if( !nr.columns[property.id] ) {
            nr.columns[property.id] = property.name;
        }

        // Записываем непустые свойства, которые удалось обработать
        if(property && data[key].length) {
            processed.properties[property.id] = data[key];
            return;
        }

        // Если найдется непустая инфа, которую не удалось обработать
        // запишем её в специальный массив
        if( nr.excluded_columns.indexOf(key) < 0 && data[key].length) {
            processed.missed_properties[key] = data[key];
        }

    }.bind(this));


    processed.images = this.getImages(processed);
    processed.description = this.getLotDescription(processed);

    return processed;
};


/**
 * Разбирает строковое название свойства
 * и возвращает объект из id и названия свойства
 *
 * @param property
 * @returns {*}
 */
NuRocker.prototype.getPropertyData = function(property){

    var regex, matches;

    // Не учитываем стандартные свойства
    if( this.excluded_columns.indexOf(property) > -1 ) {
        return null;
    }

    regex   = /(.*)\((\d+)\)$/gi;
    matches = property.match(regex);

    if(matches) {
        return {
            id: property.replace(regex, '$2'),
            name: property.replace(regex, '$1').trim()
        };
    }

    return null;
};

/**
 * Возвращает коллекцию путей к картинкам лота
 * @param lot
 * @returns {Array}
 */
NuRocker.prototype.getImages = function(lot){

    var nr = this;
    var lot_image_regex = new RegExp( '^' + lot.Id + '_(.*)\.jpg', 'i');

    var relative_path = lot.Id.match(/.{1,2}/g).slice(0,3);
    var path_to_images = path.resolve( this.data_folder, relative_path.join('/')  );


    if( fs.existsSync(path_to_images) ) {
        var images = fs.readdirSync(path_to_images).filter(function(filename){
            return lot_image_regex.test(filename);
        });

        return images.map(function(filename) {
            return path.join(nr.data_folder, filename);
        });
    }

    return [];
};

/**
 * Возвращает путь до папки с даными лота
 * @param lot
 * @returns {*}
 */
NuRocker.prototype.getLotFolder = function(lot){
    var relative_path = lot.Id.match(/.{1,2}/g).slice(0,3);

    return path.resolve( this.data_folder, relative_path.join('/')  );
};

/**
 * Возвращает коллекцию путей к картинкам лота
 * @param lot
 * @returns {Array}
 */
NuRocker.prototype.getLotImages = function(lot){

    var nr = this;
    var lot_image_regex = new RegExp( '^' + lot.Id + '_(.*)\.jpg', 'i');

    return fs.readdirSync(this.getLotFolder(lot))
        .filter(function(filename){
            return lot_image_regex.test(filename);
        })
        .map(function(filename) {
            return path.join(nr.data_folder, filename);
        });

};

/**
 * Возвращает описание лота
 * @param lot
 * @returns {Array}
 */
NuRocker.prototype.getLotDescription = function(lot){

    var nr = this;
    var desc_path = path.join( this.getLotFolder(lot), lot.Id + '_desc.txt' );

    if( fs.existsSync(desc_path) ) {
        var desc = fs.readFileSync(desc_path);

        if(desc) {
            return desc.toString();
        }
    }

    return null;
};

module.exports = NuRocker;