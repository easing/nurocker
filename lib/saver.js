var MongoClient = require('mongodb').MongoClient,
    assert      = require('assert'),
    async       = require('async'),
    _           = require('lodash')
;


/**
 * Простенький враппер для отправки данных в MongoDB
 *
 * @param url
 * @param batch_size
 * @returns {Saver}
 * @constructor
 */
var Saver = function(url, user_id, batch_size){

    assert.equal(true, !isNaN(user_id), 'неверный id пользователя');

    var saver = this;

    // Коллекция, в которую будем сохранять лоты пользователя
    this.collectionName = 'molotok_lots_' + user_id;

    // Строка соединения с монгой
    this.url = url;

    // Порция вставляемых данных
    this.batch_size = batch_size > 1 ? batch_size : 1000;


    /**
     * Разбивает входящий массив лотов на чанки
     *
     * @param lots
     * @returns [[]]
     */
    this.prepare = function(lots){
        return _.chunk(lots, this.batch_size);
    };


    /**
     * Отправляет данные в Mongo
     *
     * @param lots       - массив лотов
     * @param callback   - будет вызван после вставки всех лотов
     */
    this.insert = function(lots, callback){

        // Лоты, разбитые на порции
        var chunked = this.prepare(lots);
        var inserted_count = 0;

        MongoClient.connect(this.url, function(err, db) {

            assert.equal(null, err);

            // Коллекция, в которую будем записывать данные
            collection = db.collection(saver.collectionName);

            /**
             * Отправляет одну порцию данных в монгу
             * @param n
             * @param next
             */
            function insert (n, next) {

                var chunk = chunked[n];

                collection.insertMany(chunk, {w: 1}, function(err, result) {

                    if (err) {
                        throw err;
                        return next('insert err: ', err);
                    }

                    assert.equal(chunk.length, result.insertedCount);

                    inserted_count += result.n;

                    console.log('Порция номер [%s] записана в монгу', n);
                    console.log('Записей передано: [%s]; вставлено: [%s]', chunk.length, result.insertedCount);

                    next();
                });
            }

            // Поочередно отправляем все порции данных в базу
            async.timesSeries(chunked.length, insert, function(err) {

                if (err) {
                    return console.log('end err: ', err);
                }

                callback({
                    count: inserted_count
                });

                db.close();

            });

        });

    };

    return this;
};


module.exports = Saver;